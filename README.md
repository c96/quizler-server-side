## Quizler App - Instructor

Mobile app for the Quizler system, client/student side and server/instructor admin app.

with location checking, socket.io, and text entry to JSON (for quiz questions)

### Instructions

Navigate to app

    cd expo-apps/quizler-client

Install expo-cli globally

    npm install -g expo-cli
	
Install node packages

    npm install

Start package bundler

	npm start
	
Use emulator or device. i for iOS emulator, requires x code

### Additional

Alternate testing method

socket-tester: sample app to set up web sockets communication, use ngrok

Running the server (optional)

run ngrok http 3000 and copy the https url 
that looks something like this https://9dbe0750.ngrok.io. 
This is required because WebSockets require https.

Open quizler-client/app.js and change the SocketEndpoint 
at the top of the file to point to your endpoint.

#### HTTP server

Ngrok requires an http server; to run the http server, navigate to socket-tester and enter

    node index.js