Running the server (optional)

run ngrok http 3000 and copy the https url that looks something like this https://9dbe0750.ngrok.io. This is required because WebSockets require https.

Open quizler-client/app.js and change the SocketEndpoint at the top of the file to point to your endpoint.
