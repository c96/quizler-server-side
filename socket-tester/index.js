const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

const sqlite3 = require('sqlite3').verbose();

let db = new sqlite3.Database('./quiz-db/database/quizzesdata.db', sqlite3.OPEN_READONLY, (err) => {
  if (err) {
    return console.error(err.message);
  }
  console.log('Connected to in-memory SQLite3 database.');
})

app.get('/', function(req, res){
  res.sendfile('index.html');
});

app.post('/makerequest', function(request, response) {

    console.log("Got response: " + response.statusCode);
});

io.on('connection', function(socket){
  console.log('a user connected');

  var question_text = "--";
  var correct_answer = "--";

  // retrieve question based on user id, for now assume 1
  db.serialize(() => {
    db.each(`select * from quizzes 
      inner join users on users.group_id = quizzes.group_id
      inner join questions on questions.quiz_id = quizzes.quiz_id
      where quizzes.quiz_id = 3`, (err, row) => {
        if (err) {
            console.error(err.message);
        }
        console.log(row.user_id + "\t" + row.username + "\t" + row.quiz_name + "\t" + row.question_text)
        question_text = row.question_text;
        correct_answer = row.correct_answer;
      });
  });

  socket.emit('question', {
  	qtype: 'short-answer',
  	question: question_text,
  	correct: correct_answer
  })
  socket.on('answer', function (answer, correct) {
  	if (answer != null && answer == correct)
  	{
  		io.emit('correct');
  		console.log('Correct. Submitted: ', answer, '\nActual: ', correct);
  	}
  	else
  	{
  		io.emit('incorrect', {correct: correct})
  		console.log('Incorrect. Submitted: ', answer, '\nActual: ', correct);
  	}
  });
});

setInterval(() => {
  io.emit('ping', { data: (new Date())/1});
}, 1000);

http.listen(3000, function(){
  console.log('listening on *:3000');
});
